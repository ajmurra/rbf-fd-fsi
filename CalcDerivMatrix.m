% preallocate i,j,v arrays for use with sparse(i,j,v), we reshape these below
A_i = zeros(N,set.sten); % indices for matrices (all have the same sparsity pattern)
A_j = zeros(N,set.sten);
Ax_v = zeros(N,set.sten); % first deriv, x direction
Ay_v = zeros(N,set.sten); % first deriv, y direction
Alap_v = zeros(N,set.sten); % laplacian operator
Ahyp_v = zeros(N,set.sten); % second deriv, cross term

% extract the x and y coordinates of each stencil and store in matrix
% to ensure we can access quickly in the parallel step (i.e. sliced arrays)
sx = reshape(dom(stencils,1),[N set.sten]);
sy = reshape(dom(stencils,2),[N set.sten]);

% parallel loop through and calculate weights at each point/stencil and write to derivative matrices
parfor i = 1:N
    
    % extract x and y coords for current stencil
    stenx = sx(i,:)';
    steny = sy(i,:)';
    
    % find index of center point 
    [tf, ind_cent] = ismember(dom(i,:),[stenx, steny],'rows');
    
    if tf

        j = stencils(i,:);
        w = RBFFDWeights(ind_cent,stenx,steny,set.ord_phs,set.ord_pol,set.ord_hyp);
        
        % write weights to appropriate arrays
        A_i(i,:) = i;
        A_j(i,:) = j;
        Ax_v(i,:) = w(:,1);
        Ay_v(i,:) = w(:,2);
        Alap_v(i,:) = w(:,3);
        if set.ord_hyp > 0
            Ahyp_v(i,:) = w(:,end);
        else
            Ahyp_v(i,:) = 0;
        end
    else
        warning("couldn't find stencil center in stencil");
    end
end

% reshape the matrices into columns
sz = [1 N*set.sten];
A_i = reshape(A_i,sz)';
A_j = reshape(A_j,sz)';
Ax_v = reshape(Ax_v,sz)';
Ay_v = reshape(Ay_v,sz)';
Alap_v = reshape(Alap_v,sz)';
Ahyp_v = reshape(Ahyp_v,sz)';

% create sparse matrices
Ax = sparse(A_i,A_j,Ax_v);
Ay = sparse(A_i,A_j,Ay_v);
Alap = sparse(A_i,A_j,Alap_v);
Ahyp = sparse(A_i,A_j,Ahyp_v);

% set up matrix for solving poisson equation with built in boundary cond
Apoiss = Alap;

% set the rows to zero, since coupling should only be 1-way, i.e. from 
% boundary to domain
Apoiss(bpts_w,:) = 0;
Apoiss(bpts_i,:) = 0;
Apoiss(bpts_f,:) = 0;
Apoiss(bpts_m,:) = 0;

% set identities to setup dirichlet condition equations
Apoiss(bpts_w,bpts_w) = eye(length(bpts_w));
Apoiss(bpts_i,bpts_i) = eye(length(bpts_i));
Apoiss(bpts_f,bpts_f) = eye(length(bpts_f));
Apoiss(bpts_m,bpts_m) = eye(length(bpts_m));