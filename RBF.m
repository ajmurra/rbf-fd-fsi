function out = RBF(rbf,r,X,Xc)
%Xnorm = sqrt((X(:,1) - Xc(:,1)).^2 + (X(:,2) - Xc(:,2)).^2 + (X(:,3) - Xc(:,3)).^2);
Xnorm = norm(X-Xc);
x = Xnorm/r;
switch rbf
    case 'C0'
        out = (1-x).^2;
        if Xnorm > r
            out = 0;
        end
    case 'C2'
        out = (1 - x).^4.*(4*x + 1);
        if Xnorm > r
            out = 0;
        end
    case 'C4'
        out = (1-x).^6.*(35*x.^2+18*x+3)/3;
        if Xnorm > r
            out = 0;
        end
    case 'C6'
        out = (1-x).^8.*(32*x.^3+25*x.^2+8*x+1);
        if Xnorm > r
            out = 0;
        end
    case 'Euclid'
        out = pi*((1/12*x.^3)-0.5^2*x+4/3*0.5^3)/(pi*(-0.5^2*0+4/3*0.5^3));
        if Xnorm > r
            out = 0;
        end
    case 'Multiquadric'
        out = sqrt(1+x.^2);
    case 'InverseMulti'
        out = 1./sqrt(1+x.^2);
    case 'TPS'
        out = x.^2*log(x);
    case 'Gaussian'
        out = exp(-x.^2);
    otherwise
        error('RBF not recognised.');
end
if isnan(out)
    out = 0;
end
end