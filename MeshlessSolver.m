function MeshlessSolver(caseName)


    fprintf("-----------------\nRBF-FD FSI SOLVER\n-----------------\n")    
    % load settings
    %
    % we load up the text file using text scan
    % since this allows comments on lines with variable definitions,
    % but then we have to run through each variable and decide
    % if it's a number or not
    fid = fopen(sprintf("cases/%s/sets.txt",caseName),'r');
    set = textscan(fid,'%s %s','CommentStyle','#');
    fclose(fid);
    set = cell2struct(set{:,2},set{:,1},1);
    fn = fieldnames(set);
    for i=1:numel(fn)
        if all(ismember(set.(fn{i}),'0123456789+-.eEdD'))
            set.(fn{i}) = str2double(set.(fn{i}));
        end
    end

    fprintf("SETTINGS\n")
    disp(set);

    fprintf("--------------\nINITIALISATION\n--------------\n")
    
    % start the parallel pool
    tic
    gcp;
    fprintf("%fs\n",toc)

    % if this is a restart, just load the file 
    restart = 0;
    if isfield(set,'restartFile')
        % it may be necessary to change the output directory if we've
        % changed the directory name since the simulation ran
        [outDirTmp,~,~] = fileparts(set.restartFile);
        restart = 1;
        % save a copy of the settings file to compare to what is in the
        % restart file, in case settings have changed and we need to
        % recalculate things
        tmpSet = set;

        % load in the restart file
        fprintf("Loading restart file %s... ",set.restartFile)
        tic
        load(set.restartFile)        
        fprintf("%fs\n",toc)
        outDir = strcat(outDirTmp,'/');
        clear outDirTmp        
    else
        % create the directory to output files to        
        timestamp = datestr(datetime('now'),'yy-mm-dd_HHMM.SS'); 
        outDir = strcat(sprintf("output/%s/%s/",caseName,timestamp));
        mkdir(outDir);

        % load domain
        %
        % similar trick to the settings file, but no need to check
        % if things are numbers, since the data is more rigid, can convert 
        % straight to an array
        fid = fopen(sprintf("cases/%s/dom.txt",caseName),'r');
        dom = cell2mat(textscan(fid,'%f %f %f','CommentStyle','#'));  
        fclose(fid);

        % load boundary conditions
        % 
        % similar to domain, but some extra columns

        % define where to find the various things inside the bcs array
        bciID = 1;
        bciIND = 1;
        bciTYPE = 2;
        bciNORMx = 3;
        bciNORMy = 4;
        bciNORMxy = 3:4;
        bciVELx = 5;
        bciVELy = 6; 
        bciVELxy = 5:6;
        bciPSI = 7;
        bciVELn = 8;
        bciVELt = 9; 

        % define some globals to enumerate the condition types
        bcWALL = 1;
        bcINLET   = 2;
        bcFREE = 3;
        bcMOVING = 4;

        fid = fopen(sprintf("cases/%s/bcs.txt",caseName),'r');
        bcs = cell2mat(textscan(fid, '%f %f %f %f %f %f %f','CommentStyle','#'));
        fclose(fid);

        % normalise normals
        bcs(:,bciNORMxy) = bcs(:,bciNORMxy)./sqrt(sum(bcs(:,bciNORMxy).^2,2));

        % calculate normal and tangential velocity components
        tangents = [bcs(:,bciNORMy),-bcs(:,bciNORMx)];
        bcs = [bcs, dot(bcs(:,bciVELxy),bcs(:,bciNORMxy),2),dot(bcs(:,bciVELxy),tangents,2)];

        % now that we're done loading everything and matching boundary
        % conditions to points, replace point ids with indices in the boundary 
        % conditions and delete ids from domain, since indexed access is faster
        inds = [];
        for i = 1:length(bcs)
            ind = find(dom(:,1)==bcs(i,bciID));
            inds = [inds;ind];
        end
        bcs(:,bciID) = inds;
        dom = dom(:,2:3);
    end

    % display some warnings if necessary
    if (mod(set.tout,set.dt) ~= 0)
        warning("Data output delta is not divisible by time delta, output may be irregular.")
    end

    if (restart && tstep*tmpSet.dt == tmpSet.T)
        warning("Restart time equals final time, no simulation will be run.")
    end

    if (set.ord_phs < 2*set.ord_hyp+1)
        warning("Should have ord_phs >= 2*ord_hyp+1.")
    end

    
    % run the simulation
    
    % if this isn't a restart, do all the initial setup, pulling out indices etc.   
    if ~restart
    
        % get the total number of points in the domain
        N = length(dom);
    
        % get indices and number of boundary points
        bpts = bcs(:,bciIND);
    
        % indices of the individual bcs within the bcs array
        bcs_w = find(bcs(:,bciTYPE)==bcWALL);
        bcs_i = find(bcs(:,bciTYPE)==bcINLET);
        bcs_f = find(bcs(:,bciTYPE)==bcFREE);
        bcs_m = find(bcs(:,bciTYPE)==bcMOVING);
    
        % indices of the individual bcs within the full domain
        bpts_w = bcs(bcs_w,bciIND);
        bpts_i = bcs(bcs_i,bciIND);
        bpts_f = bcs(bcs_f,bciIND);
        bpts_m = bcs(bcs_m,bciIND);    
    
        % find indices of all volume points
        pts_vol = 1:size(dom,1);
        pts_vol([bpts_w;bpts_i;bpts_f;bpts_m]) = [];
    
        % store the dirichlet bcs for psi so we don't have to pull it out every
        % loop
        psi_w = bcs(bcs_w,bciPSI);
        psi_i = bcs(bcs_i,bciPSI);
        psi_f = bcs(bcs_f,bciPSI);
        psi_m0 = bcs(bcs_m,bciPSI);
        psi_m1 = zeros(size(psi_m0));
    
        if (length(bpts_w)+length(bpts_i)+length(bpts_f)+length(bpts_m) ~= length(bpts))
            warning("individual boundaries do to not sum to total");
        end
    end

    % if new sim, or the stencil size has changed
    if ~restart || (tmpSet.sten ~= set.sten)
        % calculate stencils
        if restart
            set.sten = tmpSet.sten;
        end
        stencils = GetNearest(dom,set.sten);
    end
    
    % if new sim, or any of the RBF-FD settings have changed
    if ~restart || (tmpSet.ord_phs ~= set.ord_phs) ...
                || (tmpSet.ord_pol ~= set.ord_pol) ...
                || (tmpSet.ord_hyp ~= set.ord_hyp)
        % calculate derivative matrices / RBF-FD weights
        fprintf("Calculating RBF-FD weights... ")
        tic
        CalcDerivMatrix;
        fprintf("%fs\n",toc)
    end

    % if new sim with fsi, or restart where we have switched on fsi
    if (~restart && set.fsi == 1) || (restart && set.fsi == 0 && tmpSet.fsi == 1)
        % take a copy of domain at time 0 for keeping track of motion
        dom0 = dom;
    
        % setup motion matrix H
    
        % stack the boundary points in order
        bpts_ordered = [bpts_m;bpts_w;bpts_f;bpts_i];
        nCtl = length(bpts);
        nVol = size(dom,1) - length(bpts);
        Css = zeros(nCtl);
        Csv = zeros(nVol,nCtl);
    
        % generate Css matrix
        fprintf("Generating Css matrix for point motion... ")
        tic
        for i = 1:nCtl
            for j = 1:nCtl
                indi = bpts_ordered(i);
                indj = bpts_ordered(j);
                Css(i,j) = RBF('C2',set.r,dom(indi,:),dom(indj,:));
            end
        end
        fprintf("%fs\n",toc)
    
        fprintf("Generating Csv matrix for point motion... ")
        tic
        parfor i = 1:nVol
            for j = 1:nCtl
                indi = pts_vol(i);
                indj = bpts_ordered(j);
                Csv(i,j) = RBF('C2',set.r,dom(indi,:),dom(indj,:));
            end
        end
        fprintf("%fs\n",toc)
    
        fprintf("Inverting Css matrix... ")
        tic
        H = Csv*inv(Css);
        fprintf("%fs\n",toc)

        % initial variables for structural solve        
        xt = 0;
        xtm1 = 0;
        yt = 0;
        ytm1 = 0;
    end

    % if we're restarting, overwrite the settings in the restart file with the ones in the
    % configuration file, and delete the temp settings
    % otherwise, set up all the initial conds etc.
    if restart
        clear tmpSet.restartFile; % don't want a restart file in a restart file
        set = tmpSet; 
        clear tmpSet restart;
    else 
        % STEP 0: initialise the simulation
    
        % set psi to be zero everywhere to begin, boundary conditions will be 
        % applied once the sim is running
        psi = zeros(N,1);
    
        % set omega to be zero everywhere except at walls, where we use
        % the boundary velocities to calculate vorticity, which then will
        % carry the boundary velocity into the domain        
        omega = zeros(N,1);        
    
        % set initial velocities zero everywhere, boundaries get set during loop
        vel = zeros(N,2);
    
        % create an identity matrix for the velocity transport equations so we
        % don't have to generate every loop
        eye4vte = eye(length(bpts));       
    
        % start at time = 0
        tstep = 0;        
        
        % write a file with the initial settings, so we always know 
        % what settings this file was run with, and we don't have to
        % recalculate everything above
        clear set.restartFile; 
        clear restart; 
        filename = strcat(outDir,'restart_tstep0.mat');
        fprintf("Writing initial data to %s... ",filename)
        tic
        save(filename);  
        fprintf("%fs\n",toc)
    end   

    % if we're on a desktop, create a plot
    if ispc
        figure
    end

    fprintf("----------------\nSIMULATION START\n----------------\n")
    while tstep*set.dt < set.T
        
        % solve poisson equation for psi, setting psi @ boundary.
        % note that the dirichlet boundary condition for psi is in omega for
        % the poisson equation, i.e. this is NOT a vorticity boundary condition
        % since the Apoiss matrix is modified to be identity at these points.
        % the vorticity boundary condition is calculated in the NEXT step.
        om = -omega;
        psi_m = psi_m0 + psi_m1;
        om(bpts_w) = psi_w;
        om(bpts_i) = psi_i;
        om(bpts_f) = psi_f;
        om(bpts_m) = psi_m;
        % solve the equation
        psi = Apoiss\om;
        % set psi on the boundary again for good measure
        psi(bpts_w) = psi_w;
        psi(bpts_i) = psi_i;
        psi(bpts_f) = psi_f;
        psi(bpts_m) = psi_m;
        
        % calculate omega at walls from psi, use Jensen's approximation
        qpts1 = dom([bpts_w;bpts_m],:)+set.h*bcs([bcs_w;bcs_m],bciNORMxy);
        qpts2 = dom([bpts_w;bpts_m],:)+2*set.h*bcs([bcs_w;bcs_m],bciNORMxy);        
        si = scatteredInterpolant(dom(:,1),dom(:,2),psi);
        si.Method = 'natural';
        psi_int1 = si(qpts1(:,1),qpts1(:,2));
        psi_int2 = si(qpts2(:,1),qpts2(:,2));
        % thom
        omega([bpts_w;bpts_m]) = 2/set.h^2*([psi_w;psi_m] - psi_int1 + set.h*bcs([bcs_w;bcs_m],bciVELt));
        % jensen
        %omega([bpts_w;bpts_m]) = 1/2/set.h^2*(7*[psi_w;psi_m]-8*psi_int1 + psi_int2 + 6*set.h*bcs([bcs_w;bcs_m],bciVELt));
        omega([bpts_i;bpts_f]) = 0;
    
        % calculate velocities
        vel(:,1) = Ay*psi;
        vel(:,2) = -Ax*psi;

        % set velocities at moving walls
        vel(bpts_m,:) = bcs(bcs_m,bciVELxy);
        
        % create diagonal velocity matrices
        dx = spdiags(vel(:,1),0,N,N);
        dy = spdiags(vel(:,2),0,N,N);
    
        % build velocity transport equation matrix
        vte = 1/set.Re*Alap - dx*Ax - dy*Ay - N^-set.ord_hyp*Ahyp;
        
        % set boundaries in matrix
        vte(bpts,bpts) = eye4vte;
        
        % step in time using RK4
        k1 = vte*omega;
        k2 = vte*(omega+set.dt*k1/2);
        k3 = vte*(omega+set.dt*k2/2);
        k4 = vte*(omega+set.dt*k3);
        omega = omega + set.dt/6*(k1 + 2*k2 + 2*k3 + k4);
        tstep = tstep + 1;
        t = tstep*set.dt;
        
        % if fsi is enabled, move the boundary    
        if (set.fsi == 1 && t >= set.fsi_start)
    
            % create interpolator before we move the points
            si_omega = scatteredInterpolant(dom(:,1),dom(:,2),omega,'natural');            
    
            % calculate pressure around moving walls
            qpts = dom(bpts_m,:)+set.hv*bcs(bcs_m,bciNORMxy);
            si_velx = scatteredInterpolant(dom(:,1),dom(:,2),vel(:,1),'natural');
            si_vely = scatteredInterpolant(dom(:,1),dom(:,2),vel(:,2),'natural');
            velx = si_velx(qpts(:,1),qpts(:,2));
            vely = si_vely(qpts(:,1),qpts(:,2));
    
            % calculate forces by summing pressures in normal directions
            % (negative since pressure force works against normal)
            F = -sum((velx.^2+vely.^2).*bcs(bcs_m,bciNORMxy));    
    
            % step the structural solver
            xtp1 = set.fsi_x*StructuralSolve(xt,xtm1,set.freq_x,set.damp_x,set.dt,F(1));
            ytp1 = set.fsi_y*StructuralSolve(yt,ytm1,set.freq_y,set.damp_y,set.dt,F(2));
    
            xtm1 = xt;
            ytm1 = yt;
            xt = xtp1;
            yt = ytp1;   
    
            % calculate velocities of moving points
            vx = (xt-xtm1)/set.dt;
            vy = (yt-ytm1)/set.dt;
    
            % move points    
            dom(bpts_m,:) = dom0(bpts_m,:) + [xt,yt];
            displacements = [repmat([xt,yt],length(bpts_m),1);zeros(length(bpts)-length(bpts_m),2)];
            dom(pts_vol,:) = dom0(pts_vol,:) + H*displacements;
    
            % interpolate omega to new points
            omega = si_omega(dom(:,1),dom(:,2));
    
            % recalculate derivative matrices
            CalcDerivMatrix;
    
            % set tangential and normal velocity at the walls
            bcs(bcs_m,bciVELxy) = repmat([vx,vy],length(bcs_m),1);
            bcs(bcs_m,bciVELn) = dot(bcs(bcs_m,bciVELxy),bcs(bcs_m,bciNORMxy),2);
            bcs(bcs_m,bciVELt) = dot(bcs(bcs_m,bciVELxy),tangents(bcs_m,:),2);        
    
            % shift moving points to origin and skew in z direction to
            % calculate boundary condition for psi
            pts = dom(bpts_m,:) - [set.cenx+xt,set.ceny+yt];
            psi_m1 = vy*pts(:,1) + vx*pts(:,2);
            
            fprintf("step = %i \tt = %f \tforce = [%d,%d] \tdisplacement = [%d,%d]\n",tstep,t,F(1),F(2),xt,yt);
        else
            fprintf("step = %i \tt = %f\n",tstep,t);
        end
        
        % if we're on desktop, plot the streamfunction and vorticity
        if (ispc && mod(t,set.tout) < set.dt/2)
            subplot(1,2,1)
            plot3(dom(:,1),dom(:,2),omega,'k.')
        
            subplot(1,2,2)
            plot3(dom(:,1),dom(:,2),psi,'k.')
        
            pause(0)
        end        

        % output data    
        if (mod(t,set.tout) < set.dt/2)
            filename = strcat(outDir,num2str(tstep),'.dat');
            fprintf("Writing output to %s... ",filename)            
            fileID = fopen(filename,'w');
            fprintf(fileID,'%f %f %f %f %f %f\n',[dom,vel(:,1),vel(:,2),omega,psi]');
            fclose(fileID);         
            fprintf("%fs\n",toc)
            if exist('F','var')
                fileID = fopen(strcat(outDir,"structural.dat"),"a+");
                fprintf(fileID,"%i %f %f %f %f %f\n",[tstep t F(1) F(2) xt, yt]);
                fclose(fileID);
            end
        end
    
        % check for nan
        if isnan(omega+psi)
            fprintf("Solution contains NaN\n")
            break;
        end        
    end
    fprintf("--------------\nSIMULATION END\n--------------\n")

    % create a restart file at end of simulation
    clear set.restartFile; 
    clear restart; 
    filename = strcat(outDir,'restart_tstep', string(tstep),".mat");
    fprintf("Writing restart data to %s... ",filename)
    tic
    save(filename);  
    fprintf("%fs\n",toc)    
end
