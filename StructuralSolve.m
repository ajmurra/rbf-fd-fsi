% returns the displacement of mass-spring-damper system at  x^(t+1) when 
% given the current displacment x^t 
% and displacement at last timestep x^(t-1)
% uses second order central difference scheme

% xtp1 = x^(t+1), return value for new displacement
% xt = x^t, current displacement
% xtm1 = x^(t-1), displacement at last timestep
% dt = time delta
% omega_n = natural frequency
% zeta = damping ratio
% F = external force

function xtp1 = StructuralSolve(xt,xtm1,omega_n,zeta,dt,F)
    c0 = 1 + dt*zeta*omega_n;
    c1 = dt*dt;
    c2 = dt*zeta*omega_n - 1;
    c3 = 2 - dt*dt*omega_n*omega_n;
    xtp1 = (c1*F + c2*xtm1 + c3*xt)/c0; 
end