function w = RBFFDWeights (i,x,y,m,d,k)

% this function is adapted from 
% 
% "Enhancing finite differences with radial basis functions:
%  Experiments on the Navier-Stokes equations"
%
% by Natasha Flyer, Gregory A. Barnett, Louis J. Wicker, 2016

% Input parameters
% x,y   Column vectors with stencil node locations; approximation to
%       be accurate at x(1),y(1)
%   m   Power of r in RBF fi(r) = r^m, with m odd, >= 3.
%   d   Degree of supplementary polynomials (d = -1 no polynomials)
%   k   Degree of hyperviscosity term
%
% Output parameter
%   w   Matrix with three columns, containing weights for d/dx, d/dy,
%       and the Laplacian d2/dx2+d2/dy2, respectively.

% Shift nodes so stencil centered at origin
x = x-x(i);
y = y-y(i);
n = length(x);


% ------ RBF part --------------------------------------------------------
% calculate matrix of distances from each point to each other point
% note this is invariant under the translation to the origin above
dists = hypot(bsxfun(@minus,x,x'),bsxfun(@minus,y,y'));

% calculate matrix of r^m
A0 = dists.^m;

% RBF matrix

% radius is just distance to origin, which is the center of the stencil
% since we translated above, so use that rather than recalculate
r = dists(:,1);

L0 = m*(bsxfun(@times,r.^(m-2),...
    [...
     -x,... % dx
     -y,... % dy
     m*ones(n,1),... % laplacian
     ])); % RHSs
 
if k > 0
     hyperv = 1;
     for i=1:k
         hyperv = hyperv*(m-(2*i-1)).^2;
     end
     hyperv = hyperv.*r.^(m-2*k); 
     L0 = [L0,hyperv];
end
% we can get a divide by zero above, but these should be zero
L0(isnan(L0))=0;

% ------ Polynomial part -------------------------------------------------
if d == -1
    % Special case; no polynomial terms,
    % i.e. pure RBF
    A = A0;
    L = L0;
else
    % Create matrix with polynomial terms and matching constraints
    X       = x(:,ones(1,d+1));
    X(:,1)  = 1;
    X       = cumprod( X,2);
    Y       = y(:,ones(1,d+1));
    Y(:,1)  = 1;
    Y       = cumprod( Y,2);
    np      = (d+1)*(d+2)/2; % Number of polynomial terms
    
    XY = zeros(n,np); col = 1; % Assemble polynomial matrix block
    
    for k = 0:d
        XY(:,col:col+k) = X(:,k+1:-1:1).*Y(:,1:k+1);
        col = col+k+1;
    end
    
    L1 = zeros(np,length(L0(1,:))); % Create matching RHSs
    
    if d >= 1
        L1(2,1) = 1; 
        L1(3,2) = 1; 
    end
    if d >= 2
        L1(4,3) = 2; 
        L1(6,3) = 2; 
    end
    
    A = [A0,XY;XY',zeros(col-1)]; % Assemble linear system to be solved
    L = [L0;L1]; % Assemble RHSs
    
end
% ------ Solve for weights -----------------------------------------------
W = A\L;
w = W(1:n,:); % Extract the RBF-FD weights
end
