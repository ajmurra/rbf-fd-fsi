function [x,y] = fill_trap(s1,s2,h,ptCnt,er)

rise = h;
run = (s2-s1)/2;
slope = rise/run;

% calculate initial spacing
x = linspace(-s1/2,s1/2,ptCnt);
spc = (x(2)-x(1));

% calculate y spacing
y = 0;
ex = er;
while max(y) <= h
    y = [y,max(y)+spc*ex];
    ex = ex*er;
end



% if removing a row would get us closer to the height, do it
if y(end)-h > h-y(end-1)
    y = y(1:end-1);
end

% rescale y
y = y/max(y)*h;

% figure 
% hold on
% plot(y*0,y,'bo')

% now go through generate the points
x = linspace(-s1/2,s1/2,ptCnt)';
out = [x,x*0+y(1)];
for i = 2:length(y)
    len = s1+2*y(i)/slope;
    spc = y(i)-y(i-1);
    num = round(len/spc)+1;
    x = linspace(-len/2,len/2,num)';
    out = [out;x,x*0+y(i)];   
end

out = unique(out,'rows');
x = out(:,1);
y = out(:,2);

% plot(out(:,1),out(:,2),'k.')
% axis equal

end